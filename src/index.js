const path = require('path');
const fs = require('fs');
const csvToJson = require('convert-csv-to-json');
const chartExporter = require("highcharts-export-server");
const PDFDocument = require('pdfkit');

const CSV_INUT = path.resolve(__dirname, "./../resource/Anamese.csv");
const JSON_OUTPUT = path.resolve(__dirname, "./../output/Anamese_from_numbers.json");
const OUTPUT_DIR = path.resolve(__dirname, "./../output");
const GRAPH_IMG_DIR = path.resolve(OUTPUT_DIR, "img");
const PDF_OUTPUT = path.resolve(OUTPUT_DIR, "report.pdf");

const parseCsvToJson = () => {
    let json = csvToJson.getJsonFromCsv(CSV_INUT);
    fs.writeFileSync(JSON_OUTPUT, JSON.stringify(json, null, 2));
}

const testChart = async (data, name) => {
    // Initialize the exporter
    chartExporter.initPool();
    // Chart details object specifies chart type and data to plot
    const chartDetails = {
        type: "png",
        width: 1200,
        options: {
            chart: {
                type: "line"
            },
            title: {
                text: `${name}`
            },
            plotOptions: {
                series: {
                    label: {
                        connectorAllowed: false
                    },
                    pointStart: 0
                }
            },
            yAxis: {
                title: {
                    text: 'Avaliação'
                }
            },
            series: data,
        }
    };


    return new Promise((resolve, reject) => {
        // fs.readFile(filePath, options, (err, data) => {
        //   // ...
        // })
        chartExporter.export(chartDetails, (err, res) => {
            if (err) {
                reject(err);
            }
            // Get the image data (base64)
            let imageb64 = res.data;
            // Filename of the output
            let outputFile = path.resolve(GRAPH_IMG_DIR, `${name}.png`);
            // Save the image to file
            fs.writeFileSync(outputFile, imageb64, "base64");
            // await asyncFs.writeFile(outputFile, imageb64, "base64");
            console.log(`Saved image '${outputFile}'!`);
            chartExporter.killPool();
            resolve();
        });
    });
}

const separateCategories = () => {
    let arrayObject = [];
    let categories = {};
    arrayObject = JSON.parse(fs.readFileSync(JSON_OUTPUT).toString());

    //setting first category
    let currentCategory = 0;
    let categoriesKeys = Object.keys(arrayObject[0]);
    let firstCategoryName = categoriesKeys[0];

    console.log(categoriesKeys[0]);
    categories[categoriesKeys[0]] = [];
    categoriesKeys = [categoriesKeys[0]];

    arrayObject.filter((element, index) => {
        // Check if new category
        if (!element[firstCategoryName].includes(":")) {
            // console.log("Closed categories", JSON.stringify(categories[categoriesKeys[currentCategory]]));
            // console.log("New Category", element[firstCategoryName]);
            currentCategory++;
            categoriesKeys.push(element[firstCategoryName]);
            categories[categoriesKeys[currentCategory]] = [];
            return false;
        }

        // extracting data to current category
        let measureName = element[firstCategoryName]; // getting the NaN value (name)
        delete element[firstCategoryName]; // deleting the NaN entry (name)
        let measureKeys = Object.keys(element);
        let values = [];

        measureKeys.forEach((value, index) => {
            var intValue = parseInt(element[value], 10);
            Number.isNaN(intValue) ? values.push(0) : values.push(intValue);
        });

        // add values to category
        categories[categoriesKeys[currentCategory]].push({
            name: measureName,
            data: values
        });
        return true;
    });

    console.log(Object.keys(categories));
    return categories;
}

const generatePDF = () => {
    const doc = new PDFDocument;
    doc.pipe(fs.createWriteStream(PDF_OUTPUT)); // write to PDF
    // doc.pipe(res);
    // HTTP response

    // add stuff to PDF here using methods described below...
    // Scale proprotionally to the specified width
    doc.image(`${GRAPH_IMG_DIR}/0-Energiaemetabolismo.png`, 0, 15, {width: 500})
    .text('Proportional to width', 0, 0);
    doc.addPage();
    doc.image(`${GRAPH_IMG_DIR}/1-Estado emocional.png`, 0, 0, {width: 500})
    .text('Proportional to width', 0, 0);

    // finalize the PDF and end the stream
    doc.end();
}
const execute = async () => {
    parseCsvToJson();
    let categories = separateCategories();
    // testChart(categories["Estado emocional"], "Energiaemetabolismo");

    for (let index = 0; index < Object.keys(categories).length; index++) {
        let value = Object.keys(categories)[index];
        await testChart(categories[value], `${index}-${value}`);
        console.log(`Graph ${index} generated...`);
    }
    console.log("Generating PDF...");
    generatePDF();
    console.log("All done!");
}
execute().then();